const express = require('express');
const app = express();
app.use('/user',require('./routes/users.js'));

app.get('/user',(req,res) => {
  res.send('User details');
  res.status(404).send('Unknown Request');
 });

app.listen( 3000, ()=>{
  console.log('Listening to port 3000')
})